import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';
import { ClimateService } from './shared/services/climate.service';
import { AuthService } from './shared/services/auth.service';
import { PlantService } from './shared/services/plant.service';
import { ClimateCreateComponent } from './climate/climate-create/climate-create.component';
import { ClimateDetailComponent } from './climate/climate-detail/climate-detail.component';
import { ClimateListComponent } from './climate/climate-list/climate-list.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { UserSignupComponent } from './user/user-signup/user-signup.component';
import { PlantListComponent } from './plant/plant-list/plant-list.component';
import { PlantDetailComponent } from './plant/plant-detail/plant-detail.component';
import { PlantCreateComponent } from './plant/plant-create/plant-create.component';
import { ThreadListComponent } from './thread/thread-list/thread-list.component';
import { ThreadCreateComponent } from './thread/thread-create/thread-create.component';
import { ThreadDetailComponent } from './thread/thread-detail/thread-detail.component';
import { InfoComponent } from './info/info.component';



@NgModule({
  declarations: [
    AppComponent,
    ClimateCreateComponent,
    ClimateDetailComponent,
    ClimateListComponent,
    UserDetailComponent,
    UserLoginComponent,
    UserSignupComponent,
    PlantListComponent,
    PlantDetailComponent,
    PlantCreateComponent,
    ThreadListComponent,
    ThreadCreateComponent,
    ThreadDetailComponent,
    InfoComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,


  ],
  providers: [AuthService, ClimateService, PlantService, HttpClientModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
