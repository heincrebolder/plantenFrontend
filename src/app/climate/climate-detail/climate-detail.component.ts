import { Component, Input, OnInit } from '@angular/core';
import { ClimateService } from '../../shared/services/climate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Climate } from '../../shared/models/climate.model'
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-climate-detail',
  templateUrl: './climate-detail.component.html',
  styleUrls: ['./climate-detail.component.css']
})
export class ClimateDetailComponent implements OnInit {
  @Input() viewMode = false;
  @Input() currentClimate: Climate = {
    _id: null,
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: ''
  };
  message = ''
  constructor(
    public authService: AuthService,
    public climateService: ClimateService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getClimate(this.route.snapshot.params["id"]);
    }
  }
  saveClimate(): void {
    this.currentClimate.user = this.authService.getUser();
    this.climateService.create(this.currentClimate).subscribe({
      next: (res) => {
        console.log(res);
      }, error: (e) => console.error(e)
    });
  }
  getClimate(id: string): void {
    this.climateService.get(id)
      .subscribe({
        next: (data) => {
          this.currentClimate = data;
          console.log(data);
          console.log(this.currentClimate);
        }, error: (e) => console.error(e)
      });
  }
  updateClimate(): void {
    this.message = '';
    this.climateService.update(this.currentClimate._id, this.currentClimate)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'This climate was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }
  deleteClimate(): void {
    this.climateService.delete(this.currentClimate._id)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(['/climates']);
        },
        error: (e) => console.error(e)
      });
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
  editCheck(): boolean {
    if (this.authService.getUser() != null) {
      console.log(this.authService.getUser())
      console.log(this.currentClimate.user._id)
      if ((this.currentClimate.user._id) == (this.authService.getUser()) || this.currentClimate.user == this.authService.getUser()) {
        console.log("true")
        return true;
      }
    } return false;
  }
}