import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of } from 'rxjs'
import { FormsModule } from '@angular/forms'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { ClimateDetailComponent } from './climate-detail.component';

describe('ClimateDetailComponent', () => {
  let component: ClimateDetailComponent;
  let fixture: ComponentFixture<ClimateDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClimateDetailComponent],
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      schemas: [NO_ERRORS_SCHEMA]

    })
      .overrideModule(BrowserModule, { set: { entryComponents: [ClimateDetailComponent] } }).compileComponents()
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClimateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should create climates', () => {
    spyOn(component.authService, 'getUser').and.returnValue("624310a388909899f6807bd9")
    const testData = {
      "_id": "625053f0d2f958be1d8528f2",
      "name": "heeeeeeeeeeeeeeeeeet",
      "averageTemp": 12222,
      "humidity": 1,
      "user": "624310a388909899f6807bd9"
    }
    const testData2 = {
      "_id": "625053f0d2f958be1d8528f2",
      "name": "koud en nat",
      "averageTemp": 8,
      "humidity": 100,
      "user": "624310a388909899f6807bd9"
    }
    component.currentClimate = testData
    component.saveClimate();
    component.currentClimate = testData2
    component.updateClimate();
    fixture.detectChanges();
    component.getClimate("625053f0d2f958be1d8528f2")
    expect(component.currentClimate).toEqual(testData2)
    expect(component.currentClimate).toBeTruthy();
    component.climateService.delete("625053f0d2f958be1d8528f2")
  });

});
