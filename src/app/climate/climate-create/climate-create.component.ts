import { Component, OnInit } from '@angular/core';
import { Climate } from '../../shared/models/climate.model';
import { ClimateService } from '../../shared/services/climate.service';
import { AuthService } from '../../shared/services/auth.service';
@Component({
  selector: 'app-climate-create',
  templateUrl: './climate-create.component.html',
  styleUrls: ['./climate-create.component.css']
})
export class ClimateCreateComponent implements OnInit {
  user = this.authService.getUser();
  climate: Climate = {
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: null
  };
  submitted = false;
  constructor(
    public climateService: ClimateService,
    private authService: AuthService
  ) { }

  ngOnInit(): void { }
  saveClimate(): void {
    this.climate.user = this.authService.getUser();
    this.climateService.create(this.climate).subscribe({
      next: (res) => {
        console.log(res);
        this.submitted = true;
      }, error: (e) => console.error(e)
    });
  }
  newClimate(): void {
    this.submitted = false;
    this.climate = {
      name: '',
      averageTemp: undefined,
      humidity: undefined,
      user: ''
    }
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
}
