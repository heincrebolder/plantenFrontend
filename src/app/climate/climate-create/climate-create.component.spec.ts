import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of } from 'rxjs'
import { FormsModule } from '@angular/forms'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { ClimateCreateComponent } from './climate-create.component';

describe('ClimateCreateComponent', () => {
  let component: ClimateCreateComponent;
  let fixture: ComponentFixture<ClimateCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClimateCreateComponent],
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      schemas: [NO_ERRORS_SCHEMA]

    })
      .overrideModule(BrowserModule, { set: { entryComponents: [ClimateCreateComponent] } }).compileComponents()
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClimateCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should create climates', () => {
    component.climate = {
      "_id": "625053f0d2f958be1d8528f2",
      "name": "heeeeeeeeeeeeeeeeeet",
      "averageTemp": 12222,
      "humidity": 1,
      "user": "624310a388909899f6807bd9"
    }
    component.saveClimate()
    fixture.detectChanges();
    expect(component.climate.user).toBeDefined
    expect(component.climate).toBeTruthy();
    // component.climateService.delete("625053f0d2f958be1d8528f2")
  });
});
