import { Component, OnInit } from '@angular/core';
import { Climate } from '../../shared/models/climate.model'
import { ClimateService } from '../../shared/services/climate.service'
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-climate-list',
  templateUrl: './climate-list.component.html',
  styleUrls: ['./climate-list.component.css']
})
export class ClimateListComponent implements OnInit {
  climates: Climate[] = [];
  eclimate: Climate = {
    _id: undefined,
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: '',
  };
  currentClimate = this.eclimate;
  currentIndex = -1;
  name = '';
  constructor(public climateService: ClimateService, private authService: AuthService) { }

  ngOnInit(): void {
    this.retrieveClimates();
  }
  retrieveClimates(): void {
    this.climateService.getAll()
      .subscribe({
        next: (data) => {
          this.climates = data;
          // console.log(data);
        }, error: (e) => console.error(e)
      });
  }
  refreshList(): void {
    this.retrieveClimates();
    this.currentClimate = this.eclimate;
    this.currentIndex = -1;
  }
  setActiveClimate(climate: Climate, index: number): void {
    this.currentClimate = climate;
    this.currentIndex = index;
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
}
