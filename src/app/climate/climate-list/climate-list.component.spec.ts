import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of } from 'rxjs'
import { FormsModule } from '@angular/forms'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { ClimateListComponent } from './climate-list.component';


describe('ClimateListComponent', () => {
  let component: ClimateListComponent;
  let fixture: ComponentFixture<ClimateListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClimateListComponent],
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      schemas: [NO_ERRORS_SCHEMA]

    })
      .overrideModule(BrowserModule, { set: { entryComponents: [ClimateListComponent] } }).compileComponents()
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClimateListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should get all climates', () => {
    spyOn(component.climateService, 'getAll').and.returnValue(of([{
      "_id": "625053f0d2f958be1d8528f2",
      "name": "heeeeeeeeeeeeeeeeeet",
      "averageTemp": 12222,
      "humidity": 1,
      "user": {
        "_id": "624310a388909899f6807bd9",
        "name": "harry"
      },
      "__v": 0
    },
    {
      "_id": "62531623b3e28585f39b4b79",
      "name": "warum",
      "averageTemp": 12222,
      "humidity": 1,
      "user": {
        "_id": "624310a388909899f6807bd9",
        "name": "harry"
      },
      "__v": 0
    }]))
    component.retrieveClimates()
    fixture.detectChanges();
    expect(component.climates.length).toBeGreaterThan(1)
  });
  it('should get detailed info', () => {
    spyOn(component.climateService, 'get').and.returnValue(of({
      "_id": "625053f0d2f958be1d8528f2",
      "name": "heeeeeeeeeeeeeeeeeet",
      "averageTemp": 12222,
      "humidity": 1,
      "user": {
        "_id": "624310a388909899f6807bd9",
        "name": "harry"
      },
      "__v": 0
    }))
    fixture.detectChanges();
    expect(component.currentClimate._id).toBeDefined
    expect(component.currentClimate).toBeTruthy();
  });
});
