import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html',
  styleUrls: ['./user-signup.component.css']
})
export class UserSignupComponent implements OnInit {
  signupUserData = { name: '', password: '' };
  displayresult = { result: '', message: '' };
  showResultBox = false;
  msg = '';

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.showResultBox = false;
  }
  signUpUser() {
    this.authService.signUp(this.signupUserData)
      .subscribe((res: any) => {
        console.log(res);
        localStorage.setItem('token', res.token);
        localStorage.setItem('user', res.user);
        this.router.navigate(['/']);
      }, err => {
        console.log(err);
        if (err.status === 401) this.msg = JSON.stringify(err.error.Error)
        this.displayresult = {
          result: "Failed",
          message: this.msg
        };
        this.showResult();
      }
      )
  }
  showResult() {
    this.showResultBox = true;
    setTimeout(() => { this.showResultBox = false; }, 5000);
  }
}
