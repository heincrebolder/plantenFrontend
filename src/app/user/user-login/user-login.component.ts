import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service'

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  loginUserData = { name: '', password: '' };
  displayresult = { result: '', message: '' };
  showResultBox = false;

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.showResultBox = false;
  }

  loginUser() {
    this.authService.loginUser(this.loginUserData)
      .subscribe(res => {
        console.log(res);
        localStorage.setItem('token', res.token);
        localStorage.setItem('user', res.user);
        this.router.navigate(['/'])
      }, err => {
        console.log(err);
        this.displayresult = {
          result: "Failed",
          message: "Invalid credentials, try again."
        };
        this.showResult();
      }
      )
  }
  showResult() {
    this.showResultBox = true;
    setTimeout(() => { this.showResultBox = false; }, 5000);
  }

}
