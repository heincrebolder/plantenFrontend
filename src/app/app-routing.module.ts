import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClimateCreateComponent } from './climate/climate-create/climate-create.component';
import { ClimateDetailComponent } from './climate/climate-detail/climate-detail.component';
import { ClimateListComponent } from './climate/climate-list/climate-list.component';
import { PlantCreateComponent } from './plant/plant-create/plant-create.component';
import { PlantListComponent } from './plant/plant-list/plant-list.component';
import { PlantDetailComponent } from './plant/plant-detail/plant-detail.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { UserSignupComponent } from './user/user-signup/user-signup.component';
import { ThreadListComponent } from './thread/thread-list/thread-list.component';
import { ThreadCreateComponent } from './thread/thread-create/thread-create.component';
import { ThreadDetailComponent } from './thread/thread-detail/thread-detail.component';
import { InfoComponent } from './info/info.component';

const routes: Routes = [
  { path: '', redirectTo: 'info', pathMatch: 'full' },
  { path: 'login', component: UserLoginComponent },
  { path: 'signup', component: UserSignupComponent },
  { path: 'info', component: InfoComponent },

  { path: 'climates', component: ClimateListComponent },
  { path: 'climates/:id', component: ClimateDetailComponent },
  { path: 'climate/create', component: ClimateCreateComponent },

  { path: 'plants', component: PlantListComponent },
  { path: 'plants/:id', component: PlantDetailComponent },
  { path: 'plant/create', component: PlantCreateComponent },

  { path: 'threads', component: ThreadListComponent },
  { path: 'threads/:id', component: ThreadDetailComponent },
  { path: 'thread/create', component: ThreadCreateComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
