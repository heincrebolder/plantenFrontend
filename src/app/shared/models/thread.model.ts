import { Plant } from './plant.model';

export class Thread {
    constructor(
        public title: string,
        public text: string,
        public imageLink: string,
        public plant: Plant,
        public user: any,
        public comment?: any,
        public _id?: any
    ) { }
}