export class User {
    constructor(
        public name: String,
        public password?: String,
        public _id?: any
    ) { }
}
