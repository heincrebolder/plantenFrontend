import { Climate } from './climate.model';

export class Plant {
    constructor(
        public name: string,
        public description: string,
        public family: string,
        public imageLink: string,
        public waterNeed: string,
        public sunNeed: string,
        public user: any,
        public climate?: any,
        public _id?: any,
    ) { }
}
