
export class Climate {
    constructor(
        public user?: any,
        public _id?: any,
        public name?: string,
        public averageTemp?: Number,
        public humidity?: Number
    ) { }

}
