import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Thread } from '../models/thread.model';

@Injectable({
  providedIn: 'root'
})
export class ThreadService {
  baseUrl = environment.apiBaseUrl + 'thread';
  constructor(private http: HttpClient) { }
  getAll(): Observable<Thread[]> {
    return this.http.get<Thread[]>(this.baseUrl)
  }
  get(id: any): Observable<Thread> {
    return this.http.get<Thread>(this.baseUrl + '/' + id)
  }
  create(data: Thread): Observable<Thread> {
    return this.http.post<Thread>(this.baseUrl, data);
  }
  update(id: any, data: Thread): Observable<any> {
    return this.http.put<Thread>(this.baseUrl + '/' + id, data)
  }
  delete(id: any): Observable<any> {
    return this.http.delete(this.baseUrl + '/' + id)
  }
}
