import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { ThreadService } from './thread.service';

describe('ThreadService', () => {
  let service: ThreadService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule]
    });
    service = TestBed.inject(ThreadService);
  });

  it('should be created', () => {
    const service: ThreadService = TestBed.inject(ThreadService)
    expect(service).toBeTruthy();
  });
});
