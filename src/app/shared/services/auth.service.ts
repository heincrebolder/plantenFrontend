import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _signUpUrl = environment.apiBaseUrl + "user/register"
  private _loginUrl = environment.apiBaseUrl + "user/login"

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  signUp(user: any) {
    return this.http.post(this._signUpUrl, user)
  }
  loginUser(user: any) {
    return this.http.post<any>(this._loginUrl, user);
  }
  logoutUser() {
    localStorage.removeItem('token')
    localStorage.removeItem('user')
    this.router.navigate(['/']);
  }

  loggedIn() {
    return !!localStorage.getItem('token')
  }

  getToken() {
    return localStorage.getItem('token')
  }
  getUser() {
    return localStorage.getItem('user')
  }
}
