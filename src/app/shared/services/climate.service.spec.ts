import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { ClimateService } from './climate.service';

describe('ClimateService', () => {
  let service: ClimateService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [RouterTestingModule, HttpClientTestingModule] });
    service = TestBed.inject(ClimateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
