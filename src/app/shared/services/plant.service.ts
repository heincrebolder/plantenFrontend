import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Plant } from '../models/plant.model';
@Injectable({
  providedIn: 'root'
})
export class PlantService {
  baseUrl = environment.apiBaseUrl + 'plant';
  constructor(private http: HttpClient) { }
  getAll(): Observable<Plant[]> {
    return this.http.get<Plant[]>(this.baseUrl)
  }
  get(id: any): Observable<Plant> {
    return this.http.get<Plant>(this.baseUrl + '/' + id)
  }
  create(data: any): Observable<any> {
    return this.http.post(this.baseUrl, data)
  }
  update(id: any, data: any): Observable<any> {
    return this.http.put(this.baseUrl + '/' + id, data)
  }
  delete(id: any): Observable<any> {
    return this.http.delete(this.baseUrl + '/' + id)
  }
}

