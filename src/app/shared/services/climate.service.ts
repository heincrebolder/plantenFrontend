import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Climate } from '../models/climate.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClimateService {
  baseUrl = environment.apiBaseUrl + 'climate';

  constructor(private http: HttpClient) { }
  getAll(): Observable<Climate[]> {
    return this.http.get<Climate[]>(this.baseUrl)
  }
  get(id: any): Observable<Climate> {
    return this.http.get<Climate>(this.baseUrl + '/' + id)
  }
  create(data: any): Observable<any> {
    return this.http.post(this.baseUrl, data)
  }
  update(id: any, data: any): Observable<any> {
    return this.http.put(this.baseUrl + '/' + id, data)
  }
  delete(id: any): Observable<any> {
    return this.http.delete(this.baseUrl + '/' + id)
  }
}
