import { Component, OnInit } from '@angular/core';
import { Plant } from '../../shared/models/plant.model';
import { Climate } from '../../shared/models/climate.model';
import { PlantService } from '../../shared/services/plant.service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-plant-list',
  templateUrl: './plant-list.component.html',
  styleUrls: ['./plant-list.component.css']
})
export class PlantListComponent implements OnInit {
  plants?: Plant[];
  eclimate: Climate = {
    _id: null,
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: '',
  };
  emptyPlant: Plant = {
    _id: null,
    name: '',
    description: '',
    family: '',
    climate: this.eclimate,
    imageLink: '',
    sunNeed: '',
    waterNeed: '',
    user: '',
  };
  currentPlant = this.emptyPlant;
  currentIndex = -1
  name = ''
  constructor(private plantService: PlantService, private authService: AuthService) { }

  ngOnInit(): void {
    this.retrievePlants();
  }
  retrievePlants(): void {
    this.plantService.getAll().subscribe({
      next: (data) => {
        this.plants = data;
        console.log(data);
      }, error: (e) => console.error(e)
    });
  }
  refreshList(): void {
    this.retrievePlants();
    this.currentPlant = this.emptyPlant;
    this.currentIndex = -1;
  }
  setActivePlant(plant: Plant, index: number): void {
    this.currentPlant = plant;
    this.currentIndex = index;
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
}
