import { Component, OnInit, Input } from '@angular/core';
import { PlantService } from '../../shared/services/plant.service';
import { ClimateService } from '../../shared/services/climate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Plant } from '../../shared/models/plant.model';
import { Climate } from '../../shared/models/climate.model'
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-plant-detail',
  templateUrl: './plant-detail.component.html',
  styleUrls: ['./plant-detail.component.css']
})
export class PlantDetailComponent implements OnInit {
  eclimate: Climate = {
    _id: null,
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: '',
  };
  climateName: any = '';
  @Input() viewMode = false;
  @Input() currentPlant: Plant = {
    _id: '',
    name: '',
    description: '',
    family: '',
    climate: this.eclimate,
    imageLink: '',
    sunNeed: '',
    waterNeed: '',
    user: '',
  };
  message = '';
  climates: Climate[] = [];

  constructor(
    private authService: AuthService,
    private plantService: PlantService,
    private climateService: ClimateService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getPlant(this.route.snapshot.params["id"]);
      this.getClimates();
    }
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
  editCheck(): boolean {
    if (this.authService.getUser() != null) {
      console.log(this.authService.getUser())
      console.log(this.currentPlant.user._id)
      if ((this.currentPlant.user._id) == (this.authService.getUser()) || this.currentPlant.user == this.authService.getUser()) {
        console.log("true")
        return true;
      }
    } return false;
  }
  getPlant(id: string): void {
    this.plantService.get(id)
      .subscribe({
        next: (data) => {
          this.currentPlant = data;
          if (this.currentPlant.climate != undefined) {
            this.climateName = this.currentPlant.climate.name;
          }
        }, error: (e) => console.error(e)
      });
  }
  updatePlant(): void {
    this.message = '';
    this.plantService.update(this.currentPlant._id, this.currentPlant)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'This plant was updated successfully!';
        },
        error: (e) => console.error(e)
      });
  }
  deletePlant(): void {
    this.plantService.delete(this.currentPlant._id)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.router.navigate(['/plants']);
        },
        error: (e) => console.error(e)
      });
  }
  getClimates(): void {
    this.climateService.getAll().subscribe(
      res => this.climates = res,
      err => console.error(err)
    )
  }
}
