import { Component, OnInit } from '@angular/core';
import { Plant } from '../../shared/models/plant.model';
import { Climate } from '../../shared/models/climate.model';
import { PlantService } from '../../shared/services/plant.service';
import { ClimateService } from '../../shared/services/climate.service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-plant-create',
  templateUrl: './plant-create.component.html',
  styleUrls: ['./plant-create.component.css']
})
export class PlantCreateComponent implements OnInit {
  eclimate: Climate = {
    _id: undefined,
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: '',
  };
  eUser = '';
  plant: Plant = {
    _id: '',
    name: '',
    description: '',
    family: '',
    climate: this.eclimate,
    imageLink: '',
    sunNeed: '',
    waterNeed: '',
    user: this.eUser,
  };
  climates: Climate[] = [];
  submitted = false;

  constructor(
    private authService: AuthService,
    private plantService: PlantService,
    private climateService: ClimateService
  ) { }

  ngOnInit(): void { this.getClimates(); }
  savePlant(): void {
    this.plant.user = this.authService.getUser();
    // this.plant.climate = this.getClimate(this.plant.climate._id)
    this.plantService.create(this.plant).subscribe({
      next: (res) => {
        console.log(res);
        this.submitted = true;
      }, error: (e) => console.error(e)
    });
  }
  newPlant(): void {
    this.submitted = false;
    this.plant = {
      _id: '',
      name: '',
      description: '',
      family: '',
      climate: this.eclimate,
      imageLink: '',
      sunNeed: '',
      waterNeed: '',
      user: this.eUser,
    };
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
  getClimates(): void {
    this.climateService.getAll().subscribe(
      res => this.climates = res,
      err => console.error(err)
    )
  }
  getClimate(id: any): void {
    this.climateService.get(id).subscribe(
      res => this.plant.climate = res,
      err => console.error(err)
    )
  }
}

