import { Component, OnInit, Input } from '@angular/core';
import { ThreadService } from '../../shared/services/thread.service';
import { PlantService } from '../../shared/services/plant.service';
import { ClimateService } from '../../shared/services/climate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Thread } from '../../shared/models/thread.model';
import { Plant } from '../../shared/models/plant.model';
import { Climate } from '../../shared/models/climate.model'
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-thread-detail',
  templateUrl: './thread-detail.component.html',
  styleUrls: ['./thread-detail.component.css']
})
export class ThreadDetailComponent implements OnInit {
  eclimate: Climate = {
    _id: undefined,
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: '',
  };
  ePlant: Plant = {
    _id: undefined,
    name: '',
    description: '',
    family: '',
    climate: this.eclimate,
    imageLink: '',
    sunNeed: '',
    waterNeed: '',
    user: '',
  };
  eThread: Thread = {
    _id: '',
    title: '',
    text: '',
    imageLink: '',
    comment: undefined,
    plant: this.ePlant,
    user: '',
  };
  message = '';
  plants: Plant[] = [];
  @Input() viewMode = false;
  @Input() currentThread: Thread = this.eThread

  constructor(
    private authService: AuthService,
    private threadService: ThreadService,
    private plantService: PlantService,
    private climateService: ClimateService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getThread(this.route.snapshot.params["id"]);
      this.getPlants();

    }
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
  editCheck(): boolean {
    if (this.authService.getUser() != null) {
      console.log(this.authService.getUser())
      console.log(this.currentThread.user._id)
      if ((this.currentThread.user._id) == (this.authService.getUser()) || this.currentThread.user == this.authService.getUser()) {
        console.log("true")
        return true;
      }
    }
    console.log("false")
    return false
  }
  getThread(id: string): void {
    this.threadService.get(id)
      .subscribe({
        next: (data) => {
          this.currentThread = data;
        }, error: (e) => console.error(e)
      });
  }
  updateThread(): void {
    this.message = '';
    this.threadService.update(this.currentThread._id, this.currentThread)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.message = res.message ? res.message : 'this thread was updated successfully!';
        }, error: (e) => console.error(e)
      });
  }
  deleteThread(): void {
    this.threadService.delete(this.currentThread._id)
      .subscribe({
        next: (res) => {
          this.router.navigate(['/threads']);
        }, error: (e) => console.error(e)
      });
  }
  getPlants(): void {
    this.plantService.getAll()
      .subscribe({
        next: (res) => {
          this.plants = res;
        }, error: (e) => console.error(e)
      });
  }
}
