import { Component, OnInit } from '@angular/core';
import { Plant } from '../../shared/models/plant.model';
import { Climate } from '../../shared/models/climate.model';
import { Thread } from '../../shared/models/thread.model';
import { PlantService } from '../../shared/services/plant.service';
import { ClimateService } from '../../shared/services/climate.service';
import { ThreadService } from '../../shared/services/thread.service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-thread-create',
  templateUrl: './thread-create.component.html',
  styleUrls: ['./thread-create.component.css']
})
export class ThreadCreateComponent implements OnInit {
  eclimate: Climate = {
    _id: null,
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: null,
  };
  ePlant: Plant = {
    _id: '',
    name: '',
    description: '',
    family: '',
    climate: this.eclimate,
    imageLink: '',
    sunNeed: '',
    waterNeed: '',
    user: '',
  };
  eThread: Thread = {
    _id: '',
    title: '',
    text: '',
    imageLink: '',
    comment: null,
    plant: this.ePlant,
    user: '',
  };
  currentThread = this.eThread;
  plants: Plant[] = [];
  submitted = false;

  constructor(
    private authService: AuthService,
    private plantService: PlantService,
    private threadService: ThreadService
  ) { }

  ngOnInit(): void { this.getPlants(); }
  saveThread(): void {
    this.currentThread.user = this.authService.getUser();
    this.threadService.create(this.currentThread).subscribe({
      next: (res) => {
        console.log(res);
        this.submitted = true;
      }, error: (e) => console.error(e)
    });
  }
  newThread(): void {
    this.submitted = false;
    this.currentThread = this.eThread;
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
  getPlants(): void {
    this.plantService.getAll().subscribe(
      res => this.plants = res,
      err => console.error(err)
    )
  }
}
