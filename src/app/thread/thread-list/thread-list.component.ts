import { Component, OnInit } from '@angular/core';
import { Climate } from '../../shared/models/climate.model';
import { User } from '../../shared/models/user.model';
import { Plant } from '../../shared/models/plant.model';
import { Thread } from '../../shared/models/thread.model';
import { ThreadService } from '../../shared/services/thread.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-thread-list',
  templateUrl: './thread-list.component.html',
  styleUrls: ['./thread-list.component.css']
})
export class ThreadListComponent implements OnInit {
  threads: Thread[] = [];

  eclimate: Climate = {
    _id: null,
    name: '',
    averageTemp: undefined,
    humidity: undefined,
    user: '',
  };
  ePlant: Plant = {
    _id: null,
    name: '',
    description: '',
    family: '',
    climate: this.eclimate,
    imageLink: '',
    sunNeed: '',
    waterNeed: '',
    user: '',
  };
  eThread: Thread = {
    _id: null,
    title: '',
    text: '',
    imageLink: '',
    comment: null,
    plant: this.ePlant,
    user: '',
  }
  currentThread = this.eThread;
  currentIndex = -1;
  title = '';
  constructor(
    private threadService: ThreadService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.retrieveThreads();
  }
  retrieveThreads(): void {
    this.threadService.getAll().subscribe({
      next: (data) => {
        this.threads = data;
        console.log(data);
      }, error: (e) => console.error(e)
    });
  }
  refreshList(): void {
    this.retrieveThreads();
    this.currentThread = this.eThread;
    this.currentIndex = -1;
  }
  setActiveThread(thread: Thread, index: number): void {
    this.currentThread = thread;
    this.currentIndex = index;
  }
  loginCheck(): boolean {
    return this.authService.loggedIn()
  }
}
